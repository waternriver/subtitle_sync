#!/usr/local/bin/python3 
import sys, re
from dateparser import parse
from datetime import timedelta
# usage: stsync.py <subfile/path> <sync/in/ms>
org = sys.argv[1]
sync = int(sys.argv[2])

title = org.split('.')[:-1]
ext = org.split('.')[-1]
pattern = "(?P<hour>[0-9]{2}):(?P<min>[0-9]{2}):(?P<sec>[0-9]{2}),(?P<ms>[0-9]{3}) --. (?P<ehour>[0-9]{2}):(?P<emin>[0-9]{2}):(?P<esec>[0-9]{2}),(?P<ems>[0-9]{3})"
prog = re.compile(pattern)

def time2str(dt):
    return f"{dt.strftime('%T')},{int(dt.microsecond/1000)}"

def adjust_time(timedict, sync=sync):
    start = parse(f"{timedict['hour']}:{timedict['min']}:{timedict['sec']}.{timedict['ms']}") + timedelta(milliseconds=sync)
    end = parse(f"{timedict['ehour']}:{timedict['emin']}:{timedict['esec']}.{timedict['ems']}") + timedelta(milliseconds=sync)
    return f'{time2str(start)} --> {time2str(end)}\n'

def resync(line):
    if res := prog.match(line):
        return adjust_time(res)
    else:
        return line

synced_lines = []
with open(org) as f:
    lines = f.readlines()

for line in lines:
    synced_lines.append(resync(line))

resfile = '.'.join(title)
f = open(f'[resync]{resfile}.{ext}', 'w')
for l in synced_lines:
    f.write(l)
f.close()